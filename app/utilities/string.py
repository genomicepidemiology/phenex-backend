import re
import datetime
from uuid import uuid4


# Conver camel case to underscore
def camel2underscore(value):
    value = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', value)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', value).lower()


def generate_filename(filename):
    file_extention = filename.split('.')[-1]
    normalized_uudi4 = str(uuid4()).replace('-', '')
    return normalized_uudi4 + '.' + file_extention


def generate_dated_uuid():
    date = datetime.date.today().strftime("%d%m%y")
    uuid = str(uuid4()).upper()[:6]
    return date + '-' + uuid
