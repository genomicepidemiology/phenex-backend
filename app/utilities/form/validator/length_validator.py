from wtforms import ValidationError


class LengthValidator(object):

    def __init__(self, min=None, max=None, count=None, message=None):
        self.min = min
        self.max = max
        self.count = count
        self.message = message

    def __call__(self, form, field):
        spaces_count = field.data.count(self.char)

        if (self.count):
            message = u'Field must contain exactly %i " %s" character(s)' % (self.count, self.char)
            raise ValidationError(message)

        if (self.min and self.max) and (spaces_count < self.min or spaces_count > self.max):
            message = u'Field must contain between  %i and %i " %s" character(s)' % (self.min, self.max, self.char)
            raise ValidationError(message)

        if (self.min and self.max is None) and (spaces_count < self.min):
            message = u'Field must contain more than %i " %s" character(s)' % (self.min, self.char)
            raise ValidationError(message)
