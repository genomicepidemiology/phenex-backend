from flask import request
from datetime import datetime
from app.run_app import app
from werkzeug.urls import url_encode


"""
Jinja2 custom filters
"""
@app.template_filter()
def format_date(value, format='%Y-%m-%d %H:%M'):
    return value.strftime(format)


@app.template_filter()
def substitute(value, expected=None, substitution=''):
    if value is expected:
        return substitution
    return value


"""
Jinja2 custom processors
"""
@app.context_processor
def processor():
    def active(endpoint, path):
        return 'active' if (endpoint in path) else ''

    def is_datetime(data):
        return type(data) is datetime

    return {
        'active': active,
        'is_datetime': is_datetime,
    }
