from app import mongo

DynamicEmbeddedDocument = mongo.DynamicEmbeddedDocument


class Metadata(DynamicEmbeddedDocument):

    meta = {
        'strict': False,
        'allow_inheritance': True
    }
