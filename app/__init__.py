import jinja2
from flask import Flask
from flask import url_for
from flask import session
from flask import redirect
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from flask_mongoengine import MongoEngine
from flask.cli import AppGroup
from flask_wtf.csrf import CSRFProtect
from app.constants import APP_ROOT
from app.constants import APP_NAME
from app.constants import APP_DEBUG
from app.constants import APP_SECRET
from app.constants import APP_ROUTE_PREFIX
from app.constants import APP_STATIC_DIR
from app.constants import APP_STATIC_PREFIX
from app.constants import MONGO_DB_HOST
from app.constants import MONGO_DB_PORT
from app.constants import MONGO_DB_NAME
from app.constants import MONGO_DB_USERNAME
from app.constants import MONGO_DB_PASSWORD
from app.constants import MIN_CODE_COVERAGE

mongo = MongoEngine()
bcrypt = Bcrypt()
csrf = CSRFProtect()
app_cli = AppGroup('app', help="Application utilities.")

# Default backend view
login_manager = LoginManager()
login_manager.login_view = "AdminView:login"


def register_cli():
    from app.base.commands.seed import seed
    from app.components.backend_user.backend_user_commands import create_admin


def register_views(app):
    from app.components.admin.admin_view import AdminView
    from app.components.health.health_view import HealthView
    from app.components.backend_user.backend_user_view import BackendUserView
    from app.components.frontend_user.frontend_user_view import FrontendUserView
    from app.components.frontend_user.frontend_user_metadata_view import FrontendUserMetadataView

    AdminView.register(app, route_prefix='/')
    HealthView.register(app, route_prefix='/')

    BackendUserView.register(app, route_prefix=APP_ROUTE_PREFIX)
    FrontendUserView.register(app, route_prefix=APP_ROUTE_PREFIX)
    FrontendUserMetadataView.register(app, route_prefix=APP_ROUTE_PREFIX)


def create_app(config=None):
    app = Flask(__name__, static_folder=APP_STATIC_DIR, static_url_path=APP_STATIC_PREFIX)

    # Disable trailing slashes for all requests
    app.url_map.strict_slashes = False

    app.config['DEBUG'] = APP_DEBUG
    app.config['TESTING'] = APP_DEBUG
    app.config['APP_ROOT'] = APP_ROOT
    app.config['APP_NAME'] = APP_NAME
    app.config['SECRET_KEY'] = APP_SECRET
    app.config['MIN_CODE_COVERAGE'] = MIN_CODE_COVERAGE

    host_uri = f'mongodb://{MONGO_DB_HOST}:{MONGO_DB_PORT}/{MONGO_DB_NAME}?authSource=admin'
    if MONGO_DB_PASSWORD and MONGO_DB_USERNAME:
        host_uri = f'mongodb://{MONGO_DB_USERNAME}:{MONGO_DB_PASSWORD}@{MONGO_DB_HOST}:{MONGO_DB_PORT}/{MONGO_DB_NAME}?authSource=admin'

    # NOTE: MONGODB_CONNECT or connect option fixes "MongoClient opened before fork." and Invalid databases during pytest run
    # I'm not aware of side effects yet. Default value is/was "True"
    app.config['MONGODB_SETTINGS'] = {
        'connect': False,
        'host': host_uri
    }

    # New templates location
    app.jinja_loader = jinja2.ChoiceLoader([
        app.jinja_loader,
        jinja2.FileSystemLoader(APP_ROOT + '/app/base/templates'),
        jinja2.FileSystemLoader(APP_ROOT + '/app/base/templates/elements'),
        jinja2.FileSystemLoader(APP_ROOT + '/app/base/templates/errors'),
        jinja2.FileSystemLoader(APP_ROOT + '/app/components/admin/templates'),
        jinja2.FileSystemLoader(APP_ROOT + '/app/components/frontend_user/templates'),
        jinja2.FileSystemLoader(APP_ROOT + '/app/components/backend_user/templates'),
    ])

    if config is not None:
        app.config.from_mapping(config)

    csrf.init_app(app)
    mongo.init_app(app)
    bcrypt.init_app(app)
    login_manager.init_app(app)

    register_cli()
    register_views(app)
    app.cli.add_command(app_cli)

    return app

# Import Models
from app.components.backend_user.backend_user_model import BackendUser


# Login manager authorized callback
@login_manager.user_loader
def load_user(user_id):
    try:
        return BackendUser.objects.get(id=user_id)
    except mongo.DoesNotExist:
        session.clear()


# Login manager unauthorized callback
@login_manager.unauthorized_handler
def unauthorized():
    return redirect(url_for('AdminView:login'))
