import app
from flask import flash
from flask import url_for
from flask import request
from flask import redirect
from flask import render_template
from datetime import datetime
from datetime import timedelta
from flask_login import login_user
from flask_login import logout_user
from flask_login import login_required
from flask_login import current_user
from flask_classful import route
from flask_classful import FlaskView
from app.components.backend_user.backend_user_repository import BackendUserRepository
from app.components.backend_user.backend_user_form import BackendUserProfileForm
from app.constants import LOGIN_MINUTES_BAN
from app.constants import LOGIN_MAX_ATTEMPTS


class AdminView(FlaskView):
    view = 'AdminView'
    route_base = '/admin'
    trailing_slash = False

    def index(self):
        return redirect(url_for(self.view + ':login'))

    @route('login', methods=['GET'])
    def login(self):
        return render_template('login.html', view=self.view)

    @route('authenticate', methods=['POST'])
    def authenticate(self):
        alias = request.form['alias']
        password = request.form['password']

        user = BackendUserRepository().find_one(username=alias)
        if "@" in alias:
            user = BackendUserRepository().find_one(email=alias)

        if user is None:
            flash("Invalid username or email", "error")
            return redirect(url_for(self.view + ':login'))

        if user.login_ban_expires_at and datetime.utcnow() < user.login_ban_expires_at:
            ban_expires = user.login_ban_expires_at - datetime.utcnow()
            ban_expires_minutes = int(ban_expires.seconds / 60)
            flash("Banned user cann't login try in " + str(ban_expires_minutes) + " minutes", "error")

            return redirect(url_for(self.view + ':login'))

        if user.login_ban_expires_at and datetime.utcnow() > user.login_ban_expires_at:
            user.login_failed_count = 0
            del(user.login_ban_expires_at)
            user.save()

        if not user.is_valid_password(password):
            user.login_failed_count += 1
            attempts_left = str(LOGIN_MAX_ATTEMPTS - user.login_failed_count)
            flash("Invalid password. " + attempts_left + " attempts left.", "error")

            if user.login_failed_count == LOGIN_MAX_ATTEMPTS:
                user.login_ban_expires_at = datetime.utcnow() + timedelta(minutes=LOGIN_MINUTES_BAN)

            if user.login_ban_expires_at and datetime.utcnow() < user.login_ban_expires_at:
                flash("You've been baned. Try to login after " + str(LOGIN_MINUTES_BAN) + " minutes", "error")

            user.save()

            return redirect(url_for(self.view + ':login'))

        user.update(active=True, login_failed_count=0)

        login_user(user)

        return redirect(url_for(self.view + ':dashboard'))

    @login_required
    @route('logout', methods=['GET'])
    def logout(self):
        current_user.update(active=False)
        logout_user()

        return redirect(url_for(self.view + ':index'))

    @login_required
    @route('dashboard', methods=['GET'])
    def dashboard(self):
        return render_template('dashboard.html')

    @login_required
    @route('profile', methods=['GET', 'POST'])
    def profile(self):
        form = BackendUserProfileForm()
        context = {'form': form, 'view': self.view}
        if request.method == 'GET':
            return render_template('profile.html', **context)

        if request.method == 'POST':
            if form.validate() is False:
                return render_template('profile.html', **context)

            repo = BackendUserRepository()
            user = repo.find_one(email=current_user.email)
            user.set_password(form.data['password'])
            if not repo.update(user):
                flash("Failed to update user", "error")
                return render_template('profile.html', **context)

            flash("Password is changed", "success")
            return render_template('profile.html', **context)
