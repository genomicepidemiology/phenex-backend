import app
from flask import flash
from flask import url_for
from flask import request
from flask import redirect
from flask import render_template
from flask_login import login_required
from flask_classful import route
from flask_classful import FlaskView
from app.base.model.address import Address
from app.base.form.address_form import AddressForm
from app.base.form.address_form import AddressDeleteForm
from app.components.frontend_user.frontend_user_form import FrontendUserEditForm
from app.components.frontend_user.frontend_user_repository import FrontendUserRepository
from app.utilities.form.metadata_form_handler import MetadataFormHandler
from app.utilities.form.validator.metadata_validator import MetadataValidator


class FrontendUserMetadataView(FlaskView):
    view = 'FrontendUserMetadataView'
    parent_view = 'FrontendUserView'
    title = 'Frontend Users Metadata'
    route_base = '/frontend_users'
    trailing_slash = False

    @login_required
    @route('<id>/metadata/add', methods=['GET', 'POST'])
    def add(self, id):
        user = FrontendUserRepository().find_one(id=id)
        if not user:
            flash("Request user does't exist", "error")
            return redirect(url_for(self.parent_view + ':index'))

        context = {
            'id': id,
            'view': self.view,
            'metadata': user.metadata
        }

        metadata_form_handler = MetadataFormHandler(AddressForm, context)
        context, active_form = metadata_form_handler.extract(request.form)

        if request.method == 'GET':
            return render_template('frontend_user_metadata_add.html', **context)

        if request.method == 'POST':
            if active_form is None:
                flash("Looks like form has been manipulated", "error")
                return redirect(url_for(self.view + ':add', id=id))

            if id != request.form['id']:
                flash("Invalid request due ids missmatch", "error")
                return redirect(url_for(self.view + ':add', id=id))

            if active_form.validate() is False:
                return render_template('frontend_user_metadata_add.html', **context)

            updated_user = metadata_form_handler.bind(user, active_form)
            if not updated_user.save():
                flash("Failed to add metadata", "error")
            else:
                flash("Metadata added", "success")

            return redirect(url_for(self.view + ':add', id=id))

    @login_required
    @route('<id>/metadata/edit', methods=['GET', 'POST'])
    def edit(self, id):
        user = FrontendUserRepository().find_one(id=id)
        if not user:
            flash("Request user does't exist", "error")
            return redirect(url_for(self.parent_view + ':index'))

        context = {
            'id': id,
            'view': self.view,
            'metadata': user.metadata,
            'form_address_delete': AddressDeleteForm(id=id)
        }

        metadata_form_handler = MetadataFormHandler(AddressForm, context)
        context = metadata_form_handler.extract(from_context=True)[0]

        if request.method == 'GET':
            return render_template('frontend_user_metadata_edit.html', **context)

        if request.method == 'POST':
            if id != request.form['id']:
                flash("Invalid request due to 'ids' missmatch", "error")
                return redirect(url_for(self.view + ':edit', id=id))

            active_form = metadata_form_handler.extract(form=request.form)[1]
            if active_form.validate() is False:
                context['form_address_' + active_form.type.data] = active_form
                return render_template('frontend_user_metadata_edit.html', **context)

            updated_user = metadata_form_handler.bind(user, active_form)
            if not updated_user.save():
                flash("Failed to update user", "error")
            else:
                flash("User was updated", "success")

            return redirect(url_for(self.view + ':edit', id=id))

    @login_required
    @route('<id>/metadata/delete', methods=['POST'])
    def delete(self, id):
        if id != request.form['id']:
            flash("Delete action was prevented due to 'ids' missmatch", "error")
            return redirect(url_for(self.view + ':edit', id=id))

        delet_form = AddressDeleteForm(request.form)
        if delet_form.validate() is False:
            flash("Delete action stoped by form validation", "error")
            return redirect(url_for(self.view + ':edit', id=id))

        user = FrontendUserRepository().find_one(id=id)
        if delet_form.metadata.data and delet_form.metadata.data:
            del user.metadata[delet_form.metadata.data][delet_form.type.data]
        else:
            del user.metadata[delet_form.metadata.data]

        if not user.save():
            flash("Failed to delete metadata", "error")
        else:
            flash("Metadata deleted", "success")

        return redirect(url_for(self.view + ':edit', id=id))
