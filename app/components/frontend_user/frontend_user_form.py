from flask_wtf import FlaskForm
from wtforms import TextField
from wtforms import HiddenField
from wtforms import StringField
from wtforms import IntegerField
from wtforms import BooleanField
from wtforms import DateTimeField
from wtforms import PasswordField
from wtforms import ValidationError
from wtforms.validators import Email
from wtforms.validators import Length
from wtforms.validators import EqualTo
from wtforms.validators import NumberRange
from wtforms.validators import DataRequired
from app.utilities.form.validator.choice_validator import ChoiceValidator
from app.utilities.form.validator.is_unique_validator import IsUniqueValidator
from app.components.frontend_user.frontend_user_model import FrontendUser


class FrontendUserEditForm(FlaskForm):
    id = HiddenField('id', [
        DataRequired()
    ])
    email = TextField('email', [
        Email(),
        DataRequired(),
        IsUniqueValidator(model=FrontendUser, unique_to='id')
    ])
    verified = BooleanField('verified')
    accept_tnc = BooleanField('verified')
    verification_code = TextField('verification_code', [
        # Length(min=6, max=6)
    ])

    def clear(self):
        if hasattr(self, 'id'):
            del self.id
        if hasattr(self, 'csrf_token'):
            del self.csrf_token


class FrontendUserAddForm(FlaskForm):
    email = TextField('email', [
        Email(),
        DataRequired(),
        IsUniqueValidator(model=FrontendUser)
    ])
    password = PasswordField('New Password', [
        DataRequired(),
        Length(min=6, max=16),
        EqualTo('password_confirm', message='Passwords must match.')
    ])
    password_confirm = PasswordField('Repeat Password', [
        DataRequired(),
        Length(min=6, max=16),
        EqualTo('password', message='Passwords must match.')
    ])
    verified = BooleanField('verified')

    def clear(self):
        if hasattr(self, 'csrf_token'):
            del self.csrf_token
        if hasattr(self, 'password_confirm'):
            del self.password_confirm


class FrontendUserDeleteForm(FlaskForm):
    id = HiddenField('Id', [
        DataRequired()
    ])
    method = HiddenField('Method', [
        DataRequired(),
        ChoiceValidator(['DELETE'])
    ], default='DELETE')

    class Meta:
        csrf = True
