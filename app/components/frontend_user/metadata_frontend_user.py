from app import db
from app.model.address import Address
from app.model.metadata import Metadata
from config.constants import REGEX

MapField = db.MapField
StringField = db.StringField
EmbeddedDocumentField = db.EmbeddedDocumentField
DynamicEmbeddedDocument = db.DynamicEmbeddedDocument


class MetadataFrontendUser(Metadata):
    ip = StringField(regex=REGEX['ipv4'], min_length=7, max_length=15)
    address = MapField(field=EmbeddedDocumentField(Address))

    meta = {
        'strict': False,
        'allow_inheritance': False
    }
