from flask_wtf import FlaskForm
from wtforms import TextField
from wtforms import HiddenField
from wtforms import IntegerField
from wtforms import BooleanField
from wtforms import DateTimeField
from wtforms import PasswordField
from wtforms import ValidationError
from wtforms.validators import Email
from wtforms.validators import Length
from wtforms.validators import EqualTo
from wtforms.validators import NumberRange
from wtforms.validators import DataRequired
from wtforms.validators import InputRequired
from app.utilities.form.validator.choice_validator import ChoiceValidator
from app.utilities.form.validator.is_unique_validator import IsUniqueValidator
from app.components.backend_user.backend_user_model import BackendUser


class BackendUserProfileForm(FlaskForm):
    password = PasswordField('New Password', [
        DataRequired(),
        Length(min=6, max=16),
        EqualTo('password_confirm', message='Passwords must match.')
    ])
    password_confirm = PasswordField('Repeat Password', [
        DataRequired(),
        Length(min=6, max=16),
        EqualTo('password', message='Passwords must match.')
    ])


class BackendUserEditForm(FlaskForm):
    id = HiddenField('id', [
        DataRequired()
    ])
    email = TextField('email', [
        Email(),
        DataRequired(),
        IsUniqueValidator(model=BackendUser, unique_to='id')
    ])
    username = TextField('username', [
        DataRequired(),
        IsUniqueValidator(model=BackendUser, unique_to='id')
    ])
    verified = BooleanField('verified')
    access_level = IntegerField('access_leve', validators=[
        NumberRange(min=0, max=10),
    ])
    login_failed_count = IntegerField('login_failed_count', validators=[
        NumberRange(min=0, max=3),
    ])
    login_ban_expires_at = DateTimeField('login_failed_count', format='%Y-%m-%d %H:%M')

    def clear(self):
        if hasattr(self, 'id'):
            del self.id
        if hasattr(self, 'csrf_token'):
            del self.csrf_token


class BackendUserAddForm(FlaskForm):
    email = TextField('email', [
        Email(),
        DataRequired(),
        IsUniqueValidator(model=BackendUser)
    ])
    username = TextField('username', [
        DataRequired(),
        IsUniqueValidator(model=BackendUser)
    ])
    password = PasswordField('New Password', [
        DataRequired(),
        Length(min=6, max=16),
        EqualTo('password_confirm', message='Passwords must match.')
    ])
    password_confirm = PasswordField('Repeat Password', [
        DataRequired(),
        Length(min=6, max=16),
        EqualTo('password', message='Passwords must match.')
    ])
    verified = BooleanField('verified')
    access_level = IntegerField('access_leve', validators=[
        NumberRange(min=0, max=10),
    ])

    def clear(self):
        if hasattr(self, 'csrf_token'):
            del self.csrf_token
        if hasattr(self, 'password_confirm'):
            del self.password_confirm


class BackendUserDeleteForm(FlaskForm):
    id = HiddenField('Id', [
        DataRequired()
    ])
    method = HiddenField('Method', [
        DataRequired(),
        ChoiceValidator(['DELETE'])
    ], default='DELETE')

    class Meta:
        csrf = True
