import click
from app import app_cli
from app.components.backend_user.backend_user_form import BackendUserAddForm
from app.components.backend_user.backend_user_repository import BackendUserRepository
from flask import current_app


@app_cli.command('create_admin', help="Create admin user.")
@click.option('--email', prompt="Email", help='Email',)
@click.option('--username', prompt="Username", help='Username')
@click.option('--access_level', prompt="Access Level", help='Access Level', default=0)
@click.option('--password', prompt="Password", help='Password', hide_input=True)
@click.option('--password_confirm', prompt="Password Confirm", help='Password Confirm', hide_input=True)
def create_admin(email, username, password, password_confirm, access_level):
    data = {
        'email': email,
        'username': username,
        'verified': True,
        'access_level': access_level,
        'password': password,
        'password_confirm': password_confirm,
    }

    with current_app.test_request_context():
        form = BackendUserAddForm(data=data, meta={'csrf': False})
        if form.validate() is False:
            for field in form.errors:
                for error in form.errors[field]:
                    click.echo("Error %s: %s " % (field, error))
            return

        form.clear()

        user = BackendUserRepository().create_model(**form.data)
        user.set_password(data['password'])
        if not user.save():
            click.echo("Error: Failed to create admin user.")

    click.echo("Admin created :)")
