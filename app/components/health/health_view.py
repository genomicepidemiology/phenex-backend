import subprocess
from flask import jsonify
from flask_classful import route
from flask_classful import FlaskView
from app.constants import MONGO_DB_HOST


class HealthView(FlaskView):

    view = 'HealthView'
    route_base = '/health'

    @route('/ready', methods=['GET'])
    def ready(self):
        ping = subprocess.Popen(["ping", "-c", "1", MONGO_DB_HOST],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE
        )
        out, error = ping.communicate()
        if not out:
            msg = f'Mongo service is not ready yet. Error: {error}'
            return jsonify({'errors': [msg]}), 503

        return jsonify({
            'status': 'ready',
            'success': True
        })

    @route('/alive', methods=['GET'])
    def alive(self):
        return jsonify({
            'status': 'alive',
            'success': True
        })
