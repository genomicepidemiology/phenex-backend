// this packages were installed via npm
var del = require('del');
var argv = require('yargs').argv;
var gulp = require('gulp');
var sass = require('gulp-sass');
var gulpIf = require('gulp-if');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var gpConcat = require('gulp-concat');
var gulpCssnano = require('gulp-cssnano');
var browserSync = require('browser-sync');

var ASSETS = [
  '../resources/assets/*',
  '../resources/assets/**/*',
  '../resources/assets/**/**/*',
];

var SCSS = [
  '../resources/scss/*.scss',
  '../resources/scss/**/*.scss',
  '../resources/scss/**/**/*.scss',
];

var HTML = {
  'development': '../app/base/templates/base.development.html',
  'testing': '../app/base/templates/base.testing.html',
  'production': '../app/base/templates/base.production.html'
};

var STATIC_JS_ROOT_PATH = '../static/js';
var STATIC_CSS_ROOT_PATH = '../static/css';
var STATIC_FONTS_PATH = '../static/css/fonts';

var WATCHED_RESOURCES = [...SCSS, ...ASSETS,  ...Object.values(HTML)];

// Delete files in static dir
function clearResources() {
  return del('../static/*', {force: true});
};

// Copy filese at resource path(s)
function loadResources(callback) {
  // Fonts
  gulp.src('../node_modules/font-awesome/fonts/*').pipe(gulp.dest(STATIC_FONTS_PATH));
  gulp.src('../node_modules/bootstrap/dist/fonts/*').pipe(gulp.dest(STATIC_FONTS_PATH));

  var RESOURCES = {
    'js': {
      'production': [
        '../node_modules/jquery/dist/jquery.min.js',
        '../node_modules/bootstrap/dist/js/bootstrap.min.js',
      ],
      'development': [
        '../node_modules/jquery/dist/jquery.js',
        '../node_modules/bootstrap/dist/js/bootstrap.js',
      ]
    },
    'css': {
      'production': [
        '../node_modules/bootstrap/dist/css/bootstrap.min.css',
      ],
      'development': [
        '../node_modules/bootstrap/dist/css/bootstrap.css',
        '../node_modules/font-awesome/css/font-awesome.css',
      ]
    },
    'maps': {
      'css': [
        '../node_modules/bootstrap/dist/css/bootstrap.css.map',
        '../node_modules/font-awesome/css/font-awesome.css.map',
      ],
    },
    'app': {
      'js': [
        '../resources/app/app.js',
      ],
    }
  }

  // Handle index page
  if (process.argv.includes('--production')) {
    gulp.src(HTML['production']).pipe(rename('base.html')).pipe(gulp.dest('../app/base/templates'));
  } else if (process.argv.includes('--testing')) {
    gulp.src(HTML['testing']).pipe(rename('base.html')).pipe(gulp.dest('../app/base/templates'));
  } else if (process.argv.includes('--development')) {
    gulp.src(HTML['development']).pipe(rename('base.html')).pipe(gulp.dest('../app/base/templates'));
  } else {
    gulp.src(HTML['development']).pipe(rename('base.html')).pipe(gulp.dest('../app/base/templates'));
  }

  gulp.src(RESOURCES['app']['js'])
    .pipe(gpConcat('concat.js'))
    .pipe(gulpIf(argv.production, uglify({mangle: false})))
    .pipe(gulpIf(argv.production, rename('app.min.js'), rename('app.js')))
    .pipe(gulp.dest(STATIC_JS_ROOT_PATH));

  var libs_js = (argv.production) ? RESOURCES['js']['production'] : RESOURCES['js']['development'];
  gulp.src(libs_js)
    .pipe(gulpIf(argv.production || argv.testing, gpConcat('concat.css')))
    .pipe(gulpIf(argv.production, uglify({mangle: false})))
    .pipe(gulpIf(argv.production, rename('vendors.min.js')))
    .pipe(gulpIf(argv.testing, rename('vendors.js')))
    .pipe(gulp.dest(STATIC_JS_ROOT_PATH));

  var libs_css = (argv.production) ? RESOURCES['css']['production'] : RESOURCES['css']['development'];
  gulp.src(libs_css)
    .pipe(sass().on('error', sass.logError))
    .pipe(gulpIf(argv.production || argv.testing, gpConcat('concat.css')))
    .pipe(gulpIf(argv.production, gulpCssnano()))
    .pipe(gulpIf(argv.production, rename('vendors.min.css')))
    .pipe(gulpIf(argv.testing, rename('vendors.css')))
    .pipe(gulp.dest(STATIC_CSS_ROOT_PATH));

  var libs_maps_css = RESOURCES['maps']['css'];
  gulp.src(libs_maps_css).pipe(gulp.dest(STATIC_CSS_ROOT_PATH));

  gulp.src(SCSS)
    .pipe(sass().on('error', sass.logError))
    .pipe(gulpIf(argv.production, gulpCssnano()))
    .pipe(gulpIf(argv.production, rename('styles.min.css'), rename('styles.css')))
    .pipe(gulp.dest(STATIC_CSS_ROOT_PATH));

  callback();
}

// Reload browser on resource change
function syncBrowser(callback) {

  browserSync({
    proxy: 'http://localhost/',
    port: 3001,
    open: false,
  });

  gulp.watch(WATCHED_RESOURCES, gulp.series(loadResources)).on('change', browserSync.reload);
  callback();
}

// Load resorces on change
function changedResources(callback) {
  gulp.watch(WATCHED_RESOURCES, loadResources);
  callback();
}

exports.clear = clearResources;
exports.load = loadResources;
exports.watch = changedResources;
exports.sync = gulp.series(syncBrowser);
exports.default = gulp.series(clearResources, loadResources);
