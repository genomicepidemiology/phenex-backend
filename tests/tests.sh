#!/bin/ash

set -e

echo "Test: Check if server runs uwsgi server"
server=$(curl -sI localhost | grep "Server: ")
status_uwsgi=$(echo $server | grep "uwsgi")
if [ -z "$status_uwsgi" ]; then
  echo 'WSGI server uwsgi is not running'
  exit 1
fi

echo "Test: pytest"
coverage run -m pytest tests/

# # TODO: Enabling it once you will find the way how to test UI
# echo "Test: Check if test coverage is within minimum % coverage socre"
# COVERAGE_SCORE=$(coverage report | grep TOTAL | awk '{print $4}' | sed 's/\%//g')
# if [ "$COVERAGE_SCORE" -lt "$MIN_CODE_COVERAGE" ]; then
#   echo "Coverage Score of $COVERAGE_SCORE% is not acceptable! Required score cannot be smaller than $MIN_CODE_COVERAGE%"
#   exit 1
# fi

if [ "$DEPLOYMENT" = "development" ]; then
  echo "Test: Run selenium e2e tests"
  selenium-side-runner --timeout 2000 -s http://chromedriver:4444 tests/e2e/selenium/*.side
  echo "Finished Testing!"
fi
