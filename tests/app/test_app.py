import pytest

from app import mongo
from pythonping import ping
from app.constants import MONGO_DB_HOST
from app.constants import TESTING_MONGO_DB_NAME


def test_config(app):
    assert app.testing is True
    # assert app.config['MONGODB_DB'] == TESTING_MONGO_DB_NAME
    # check that app database is connected with correct client
    # assert mongo.get_db().name == app.config['MONGODB_DB']


def test_mongo_services():
    try:
        status = ping(MONGO_DB_HOST, count=1)
    except Exception:
        status = 'not responding'

    assert status != 'not responding'
